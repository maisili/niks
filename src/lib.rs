#![crate_name = "niks"]
#![crate_type = "rlib"]

use {
  serde::{ Deserialize, Serialize },
  uniks::{ Pod, Iuniks }
};

#[derive(Serialize, Deserialize)]
pub struct StructuredAttrs {
  pub name: String,
  pub builder: String,
  pub system: System,
  pub niks: Niks,
}

#[derive(Serialize, Deserialize)]
pub enum Niks {
  Pod(Pod),
}

#[derive(Serialize, Deserialize)]
pub enum System {
  #[serde(rename = "x86_64-linux")]
  X86_64,
  #[serde(rename = "i686-linux")]
  I686,
  #[serde(rename = "aarch64-linux")]
  AARCH64,
  #[serde(rename = "armv7l-linux")]
  ARMV7L,
  #[serde(rename = "avr-linux")]
  AVR,
}
